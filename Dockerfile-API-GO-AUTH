FROM golang:1.18 AS builder

WORKDIR /app

COPY api-go-auth .
RUN go mod tidy
RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -a -o app ./cmd/app/.

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/app /bin/app

COPY ./api-go-auth/config.example.json /config.json

COPY ./_ssl /bin/ssl
ENV SSL_PATH "/bin/ssl"

ENV LOGS_PATH "/logs"

ENV PORT 11400

EXPOSE $PORT

ENTRYPOINT ["/bin/app"]
